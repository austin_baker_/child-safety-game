Post-Apocalyptic Interior Pack

v1.1

---

This package contains various interior objects in post-apocalyptic style for bedroom, kitchen, living room, restroom and other. Objects will fit well into the atmosphere of old abandoned apartments. 

PBR ready. Most textures are 2048x2048. All meshes are highly optimized. 
Texture maps: albedo, metallic, normal, occlusion 

Totally 27 models and 37 prefabs. 

NOTE:
/Post-Apocalyptic Interior Pack/Prefabs/ folder contains ready to use objects. You can find a few demo scenes in /Post-Apocalyptic Interior Pack/Demo/Scenes folder. Demo folder can be deleted.

---

Version history

v1.1
* New objects: Office chair and office table
* Fixed lightmap problems in demo scenes

---

E-mail me at Voodoo2211@gmail.com if you have any questions or suggestion

Also, check out more useful things for Unity here: 
https://www.assetstore.unity3d.com/#/publisher/979