ReadMe.txt

Easy Voice is a Unity editor extension that helps developers quickly and easily prototype in-game voice dialog all within Unity. It uses native text to speech (TTS) to generate sound files from written dialogue lines.

Documentation can be found online at the link below:
http://game-loop.com/dev-tools


Release Notes
1.2 - Unity 5 release update. Fixed error when using certain add-on voices.
1.1 - Added template option for 0 padding filenames. Fixed compatibility with web player target.
1.0 - Initial release









Copyright (c) 2014 Game Loop