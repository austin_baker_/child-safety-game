﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextMenuController : MonoBehaviour {

    public Text textBox;


    public void updateText(string newText)
    {
        textBox.text = newText;
        // I suppose that we should make "speak" the text at this point

    }
}
