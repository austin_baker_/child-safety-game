﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerUIController : MonoBehaviour {

    private Transform Controller;
    private GameObject safeTOmeterLevel;
    public float initialLevel = 100;
    private int totalHazards = 2;
    public float decreaseRate = 0.1f;
    public Color maxColor = Color.blue;
    public Color minColor = Color.red;
    private GameObject hazardInfoPanel;
    private Image hazardToken;
    private Text hazardTitle;
    public float hazardTimeOpen = 5.0f;
    private GameObject textToSpeechPanel;
    private GameObject levelStartPanel;
    private PlayerController playerController;
    private Text missionStatusText;
    private Text startButtonText;
    public string levelDescription = "Your Mission: ";
    private Text monsterNameText;
    public AudioClip missionAudio;
    public string monsterName = "Electricity Monster";
    private Text monsterTypeText;
    public string monsterType = "Electricity";
    private Text monsterDesctiptionText;
    public string monsterDescription = "This monster is responsible for causing hazards related to electricity and shock.";
    private Image monsterImage;
    public Sprite monsterSprite;
    public bool loadData = false;

    private const float MAX_LEVEL = 100;
    private float currentLevel;
    private float initTop, initBottom, initSize;
    private RectTransform meterRectTransform;
    private Image meterImage;
    private bool hazardCanvasOn = false;
    private float startHazardCanvasTime = 0.0f;
    private bool levelComplete = false;
    private int collectedHazards = 0;


	// Use this for initialization
	void Start () 
    {
        GameObject MVC = GameObject.FindGameObjectWithTag("MVC").gameObject;
        foreach (Transform child in MVC.transform)
        {
            if (child.name == "Controller")
                Controller = child;
        }
        foreach (Transform child in Controller.transform)
        {
            if (child.name == "Player")
            {
                playerController = child.GetComponent<PlayerController>();
            }
            if(child.name == "InGameUI")
            {
                //Set SafeTOmeter
                safeTOmeterLevel = child.GetChild(0).GetChild(0).gameObject;

                //Set Text Panel
                textToSpeechPanel = child.GetChild(1).gameObject;

                
                //Set Hazard Info
                hazardInfoPanel = child.GetChild(2).gameObject;
                hazardToken = hazardInfoPanel.transform.GetChild(0).GetChild(0).GetComponent<Image>();
                hazardTitle = hazardInfoPanel.transform.GetChild(1).GetChild(0).GetComponent<Text>();

                //Set Level Start Panel
                levelStartPanel = child.GetChild(3).gameObject;
                missionStatusText = levelStartPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>();
                startButtonText = levelStartPanel.transform.GetChild(2).GetChild(0).GetComponent<Text>();
                monsterNameText = levelStartPanel.transform.GetChild(1).GetChild(3).GetChild(0).GetComponent<Text>();
                monsterTypeText = levelStartPanel.transform.GetChild(1).GetChild(5).GetChild(0).GetComponent<Text>();
                monsterDesctiptionText = levelStartPanel.transform.GetChild(1).GetChild(6).GetChild(0).GetComponent<Text>();
                monsterImage = levelStartPanel.transform.GetChild(1).GetChild(0).GetComponent<Image>();
            }
        }
        GameObject hazards = GameObject.Find("Hazards");
        if (hazards)
            totalHazards = hazards.transform.childCount;
        else
            Debug.LogError("Hazards object could not be found");

        // Initialize some variables
        meterRectTransform = safeTOmeterLevel.GetComponent<RectTransform>();
        meterImage = safeTOmeterLevel.GetComponent<Image>();
        initTop = -meterRectTransform.offsetMax.y;
        initBottom = meterRectTransform.offsetMin.y;
        initSize = meterRectTransform.localScale.y;
        levelComplete = false;

        // Turn off all unnecessary canvases
        hazardInfoPanel.SetActive(false);

        // Show level description and information
        textToSpeechPanel.SetActive(true);
        textToSpeechPanel.GetComponentInChildren<Text>().text = levelDescription;
        monsterNameText.text = monsterName;
        monsterTypeText.text = monsterType;
        monsterDesctiptionText.text = monsterDescription;
        monsterImage.sprite = monsterSprite;

        // Update the starting value of the SafeTOmeter
        UpdateSafeTOmeter(initialLevel);

        // Play the mission audio
        playTextToSpeech(missionAudio);
	}

    void Update()
    {
        if (playerController.getCanMove())
        {
            // Decrease the timer
            UpdateSafeTOmeter(currentLevel - decreaseRate * Time.deltaTime);
        }
    }

    void FixedUpdate()
    {
        // set canvas on or off
        if (hazardCanvasOn)
        {
            hazardInfoPanel.SetActive(true);
            textToSpeechPanel.SetActive(true);
            if ((Time.time - startHazardCanvasTime) > hazardTimeOpen)
            {
                hazardCanvasOn = false;
                textToSpeechPanel.SetActive(false);
            }
        }
        else
        {
            hazardInfoPanel.SetActive(false);
        }
    }
	
    // Update the size and the level of the SafeTOmeter
	public void UpdateSafeTOmeter (float newLevel)
    {
        currentLevel = newLevel;
        if (currentLevel > MAX_LEVEL)
        {
            currentLevel = MAX_LEVEL;
        }
        float newSize = initSize * (currentLevel / MAX_LEVEL);
        meterRectTransform.localScale = new Vector2(meterRectTransform.localScale.x, newSize);

        // update color of level
        meterImage.color = Color.Lerp(minColor, maxColor, currentLevel / MAX_LEVEL);

        // Check the new level
        checkLevel();

	}

    public float GetCurrentLevel()
    {
        return currentLevel;
    }

    // Check the level of the SafeTOmeter
    // If the level is Maxed - Complete the level
    // If the level is Zeroed - Fail the level
    private void checkLevel()
    {
        if (collectedHazards == totalHazards)  // Level Complete
        {
            // Update the PlayerStats values and return to the Level Select
            // First, display the ending results (Mission Status: SUCCESS)
            playerController.setCanMove(false);
            levelStartPanel.SetActive(true);
            missionStatusText.text = "COMPLETE";
            missionStatusText.color = Color.green;
            startButtonText.text = "Continue";

            // Enable ability to return to level select
            levelComplete = true;
            
            // Set PlayerStats vars (add this level index +1)
            PlayerStats.gainLevel(Application.loadedLevel + 1);
            print("A new Level was added.\n");
        }
        else if (currentLevel <= 0) // Level Failed
        {
            // Update the PlayerStats values and return to the Level Select
            // First, display the ending results (Mission Status: FAILED)
            playerController.setCanMove(false);
            levelStartPanel.SetActive(true);
            missionStatusText.text = "FAILED";
            missionStatusText.color = Color.red;
            startButtonText.text = "Continue";

            // Enable ability to return to level select
            levelComplete = true;
        }
    }

	public bool isComplete()
	{
		return true;
	}

    public void collectedToken(string title, string description, Sprite token, AudioClip sound)
    {
        // turn on the canvas elements
        hazardCanvasOn = true;
        startHazardCanvasTime = Time.time;

        // update the info in the UI elements
        hazardToken.sprite = token;
        hazardTitle.text = title;
        textToSpeechPanel.GetComponentInChildren<Text>().text = description;
        playTextToSpeech(sound);

        // update the SafeTOmeter
        collectedHazards++;
        UpdateSafeTOmeter(currentLevel + 25);

        
    }

    // Called when the Start button is pressed
    // Begins the level by deactivating the canvases and allowing the player to move
    public void beginLevel()
    {
        if (!levelComplete)
        {
            //This code is automatically called in PlayerStats Class
            /*
            if (loadData) {
                PlayerStats ps = new PlayerStats();
                ps.LoadData();
            }
             * */
            
            levelStartPanel.SetActive(false);
            textToSpeechPanel.SetActive(false);
            playerController.setCanMove(true);

        }
        else
        {
            WaitForSeconds wf = new WaitForSeconds(2.0f);
            SceneManager.LoadScene("LevelSelect");
        }
        

    }


    public void playTextToSpeech(AudioClip ac)
    {
        if (ac)
        {
            GetComponent<AudioSource>().clip = ac;
            GetComponent<AudioSource>().Play();
        }
    }

}
