﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour {

    public float movementThreshold = 0.3f;              // minimum value that the Axis value must reach for movement
    public float movementSpeed = 3.0f;                  // speed of forward and backward movement
    public float rotateSpeed = 1.0f;                    // speed of rotation
    public float gravity = 20.0f;                       // Gravity
    

	private GameObject UIController;


    public float time = 0.0f;
    private const string vInput = "Vertical";           // vertical input axis name
    private const string hInput = "Horizontal";         // horizontal input axis name
    private CharacterController charController;         // Character Controller component
    private bool canMove = true;

    private GameObject MVC;                              //The MVC Object
    private Transform Model;
    private Transform View;
    private Transform Controller;


	// Use this for initialization
	void Start () {
        charController = GetComponent<CharacterController>();

        //Load up MVC Objects for easier access
        MVC = GameObject.FindGameObjectWithTag("MVC").gameObject;
        foreach(Transform child in MVC.transform)
        {
            if (child.name == "Model")
                Model = child;
            if (child.name == "View")
                View = child;
            if (child.name == "Controller")
                Controller = child;
        }
        //Get the UI Controller
        UIController = this.transform.GetChild(3).GetComponent<PlayerUIController>().gameObject;
        // Pause the movement at the beginning of the level
        canMove = false;
	}
	
	// Update is called once per frame
	void Update () {

        //DisplayDebugInfo();

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.collider.gameObject.tag == "hazardmodel")
                {
                    Debug.Log(hit.collider.gameObject.transform.parent.name);

                    // This may need to be optimized later
                    hit.collider.gameObject.transform.parent.GetComponentInChildren<TokenClass>().collectToken();
                }
            }
        }
        
	}

	void FixedUpdate() {

        if (canMove)
        {
            Vector3 currentMovement = transform.TransformDirection(Vector3.forward);

            // Horizontal Rotation
            transform.Rotate(new Vector3(0, Input.GetAxis(hInput) * rotateSpeed, 0));

            // Vertical Movement
            currentMovement *= movementSpeed;
            currentMovement *= Input.GetAxis(vInput);

            // Apply Gravity
            currentMovement.y -= gravity * Time.deltaTime;

            // Move
            charController.Move(currentMovement * Time.deltaTime);

            //Check to see if user is clicking on an object
           

            //Add to the current time
            time += Time.deltaTime;
        }
		
	}

    private void DisplayDebugInfo()
    {
        Debug.Log("isGrounded: " + charController.isGrounded);
        Debug.Log(vInput + ": " + Input.GetAxis(vInput) + "  " + hInput + ": " + Input.GetAxis(hInput));
    }

	private void hazardInteract()
	{

	}

    public void setCanMove(bool s) {
        canMove = s;
    }

    public bool getCanMove()
    {
        return canMove;
    }

    public float getTime()
    {
        return time;
    }
}
