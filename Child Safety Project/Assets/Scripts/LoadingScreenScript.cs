﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadingScreenScript : MonoBehaviour {

    public GameObject loadingBackground;
    public GameObject loadingBar;

	// Use this for initialization
	void Start () {
        loadingBackground.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void loadLevel(string level)
    {
        StartCoroutine(DisplayLoadingScreen(level));


    }

    IEnumerator DisplayLoadingScreen(string level)
    {
        loadingBackground.SetActive(true);

        // update the size of the progress bar
        loadingBar.transform.localScale = new Vector3(0, loadingBar.transform.localScale.y, loadingBar.transform.localScale.z);

        // Start loading the next level
        AsyncOperation async = SceneManager.LoadSceneAsync(level);
        while (!async.isDone)
        {
            loadingBar.transform.localScale = new Vector3(async.progress, loadingBar.transform.localScale.y, loadingBar.transform.localScale.z);

            yield return null;
        }

        
    }

}
