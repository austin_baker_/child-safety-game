﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class LevelSelectController : MonoBehaviour {

    public int selectedLevel = 1;
    public GameObject selectionPanel;
    public GameObject levelsPanel;
    public GameObject levelSelectCanvas;
    public LoadingScreenScript loadingCanvas;
    public AudioClip[] levelClips;

    void Start()
    {
        //temporary script
        int x = PlayerStats.levelsOpen.Count;
        if (x == 0) { 
            print("Error  loading levels.");
            PlayerStats.defaults();
            x = PlayerStats.levelsOpen.Count;
    }
        print(x + "levels to be opened on menu.");
        for(int i=0; i<x; i++)
            levelsPanel.transform.GetChild(i).GetComponent<Button>().interactable = true;
    }

    public void pressPlayLevel()
    {
        levelSelectCanvas.SetActive(false);
        loadingCanvas.loadLevel("Level" + selectedLevel);
        //SceneManager.LoadScene("Level" + selectedLevel);
    }

    public void pressMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void selectLevel(int level)
    {
        selectedLevel = level;
        GetComponent<AudioSource>().clip = levelClips[selectedLevel - 1];
        GetComponent<AudioSource>().Play();
        selectionPanel.transform.position = levelsPanel.transform.GetChild(selectedLevel - 1).transform.position;
    }

    public void pressForward()
    {

    }

    public void pressBack()
    {

    }
}
