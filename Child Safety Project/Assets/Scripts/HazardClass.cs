﻿using UnityEngine;
using System.Collections;


public class HazardClass : MonoBehaviour {

    //The token or tokens related to this hazard
    public TokenClass[] relatedTokens;
    public string type;
    public float life;

    void Start()
    {
        life = Time.time;
    }//End Start

    //function to compute how long it took to solve
    public float grabLife()
    {

        return Time.time - life;
    }

	
}
