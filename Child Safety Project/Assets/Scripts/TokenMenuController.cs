﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class TokenMenuController : MonoBehaviour {

    public GameObject tokensPanel;
    public Text tokenTitle;
    public Image tokenImage;
    public Sprite[] tokens;
    public Button[] buttons;
    public Text threatLevel;
    public Text tokenDescription;

	// Use this for initialization
	void Start () {
	    // Gather all of the token information from the static classes
        if(PlayerStats.achievementWall.Contains("Fire"))
        {
            buttons[0].image.sprite = tokens[0];
            buttons[0].interactable = true;
        }
        if (PlayerStats.achievementWall.Contains("Poison"))
        {
            buttons[1].image.sprite = tokens[1];
            buttons[1].interactable = true;
        }
        if (PlayerStats.achievementWall.Contains("Drowning"))
        {
            buttons[2].image.sprite = tokens[2];
            buttons[2].interactable = true;
        }
        if (PlayerStats.achievementWall.Contains("Suffocation&Choking"))
        {
            buttons[3].image.sprite = tokens[3];
            buttons[3].interactable = true;
        }
        if (PlayerStats.achievementWall.Contains("Puncture"))
        {
            buttons[4].image.sprite = tokens[4];
            buttons[4].interactable = true;
        }
        if (PlayerStats.achievementWall.Contains("Electric"))
        {
            buttons[5].image.sprite = tokens[5];
            buttons[5].interactable = true;
        }
        if (PlayerStats.achievementWall.Contains("Falls&Trips"))
        {
            buttons[6].image.sprite = tokens[6];
            buttons[6].interactable = true;
        }

	}


    public void selectToken(string tokenName)
    {
        // use the token name to search a static dictionary of all tokens information
        switch (tokenName)
        {
            case "Fire":
                {
                    tokenImage.sprite = tokens[0];
                    tokenTitle.text = "Fire Token";
                    tokenDescription.text = "Fire hazards can cause intense burns on skin and may continue to cause other damage to the house. Typical items include open flames, plugged in electronics and torn power cords.";
                    break;
                }
            case "Poison":
                {
                    tokenImage.sprite = tokens[1];
                    tokenTitle.text = "Poison Token";
                    tokenDescription.text = "Poison hazards can cause people or animals to become very sick or die. Typical items include medicine, cleaning products and old garbage.";
                    break;
                }
            case "Drowning":
                {
                    tokenImage.sprite = tokens[2];
                    tokenTitle.text = "Drowning Token";
                    tokenDescription.text = "Drowning hazards are very dangerous and are caused by open sources of water. Typical items include water buckets, water left in a bathtub and open toilets. ";
                    break;
                }
            case "Choke":
                {
                    tokenImage.sprite = tokens[3];
                    tokenTitle.text = "Suffocation & Choking Token";
                    tokenDescription.text = "Choking hazards can cause people or animals loose the ability to breathe. Typical items include plastic bags, loose cords and small objects.";
                    break;
                }
            case "Puncture":
                {
                    tokenImage.sprite = tokens[4];
                    tokenTitle.text = "Puncture Token";
                    tokenDescription.text = "Puncture hazards can cause a hole or wound made by a sharp point. Typical items include pens, knifes and even the corner of chairs and tables.";
                    break;
                }
            case "Electric":
                {
                    tokenImage.sprite = tokens[5];
                    tokenTitle.text = "Electric Token";
                    tokenDescription.text = "Electric hazards are caused by coming in contact with open wires. Typical items include uncovered outlets and torn power cords";
                    break;
                }
            case "Trip":
                {
                    tokenImage.sprite = tokens[6];
                    tokenTitle.text = "Falls & Trip Token";
                    tokenDescription.text = "Falling hazards can be caused by cluttered junk, wet surfaces and loose furniture.";
                    break;
                }
        }

    }


    public void returnToMain()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
