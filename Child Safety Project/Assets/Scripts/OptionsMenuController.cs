﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class OptionsMenuController : MonoBehaviour {

    public GameObject LoginPanel;
    public GameObject HiddenPanel;

    // We will need to discuss how to store and handle the password further
    public string defaultPassword = "SeniorDesignTeam6";
    public string defaultDest = "Not sure about this";

	// Use this for initialization
	void Start () {
        LoginPanel.SetActive(true);
        HiddenPanel.SetActive(false);
	}


    public void submitPassword(InputField pass)
    {
        // Validate password and turn on Hidden Panel
        if(pass.text == defaultPassword) {
            activateHiddenPanel();
        }
        else
        {
            // TODO: failure to validate password
        }

    }

    // Turns on the Hidden Options panel and
    // loads the previous data for username and 
    // store location
    private void activateHiddenPanel()
    {
        LoginPanel.SetActive(false);
        HiddenPanel.SetActive(true);
        // TODO: Load in previously assigned name from static classes
    }

    public void returnToMain()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
