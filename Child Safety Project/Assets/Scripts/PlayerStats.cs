﻿using UnityEngine;
using System.Collections;
using System.IO;
using saveFile;
using loadFile;
using System;

public class PlayerStats : MonoBehaviour
{
    //Instance Variables
    public static ArrayList achievementWall;           //List containing all collected hazard types
    public static ArrayList currentHazardsCollected;  // current list of hazard information (research data)
    public static ArrayList activeHazards;            //list of current hazards in level
    public static ArrayList levelsOpen;               //list of unlocked levels
    public static int activeScene;                    //index of current scene


    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(this);
        currentHazardsCollected = new ArrayList();
        achievementWall = new ArrayList();
        levelsOpen = new ArrayList();
        LoadData();
    }//End Start

    // Update is called once per frame
    void Update() { }//End Update



    //When a new scene is loaded
    void OnLevelWasLoaded(int level)
    {
        setScene(Application.loadedLevel);
        Debug.Log("PlayerStats.OnLevelWasLoaded("+level.ToString()+")");
        if (Application.loadedLevel == 1 && levelsOpen == null)
        {//Level Select Screen and array is empty
            defaults();
            print("Defaults have been Loaded.\n");
        }
        else if (Application.loadedLevel > 5)
        {       //Only Run Scripts if Level is functional and not a menu or test level
            grabHazards();
            setScene(Application.loadedLevel);

            if (currentHazardsCollected != null)   // New Level and not the first time through
            {
                currentHazardsCollected = new ArrayList();      //clean current list
                //Debug for adding more arrays to the array list
                print("A new array was added to the main hazards list, a new array has been created.\n");
            }
            print("A new Level was Loaded.\n");
        }
    }//End OnLevelWasLoaded


    /*
    //When the Application Exits
    void OnApplicationQuit()
    {
        SaveData();
    }//End On Application Quit

    */

    //Constructor
    public PlayerStats()
    {
        activeHazards = new ArrayList(); levelsOpen = new ArrayList(); //LoadData();
    }//End Constructor
    //===================Main Functions================//



    //Called when you achieve a new Level
    public static void gainLevel(int x)
    {
        if (!levelsOpen.Contains(x))
        {
            levelsOpen.Add(x);
            print("A new Level was gained.\n");
        }
        else
            print("The next level is already unlocked.\n");
        print(levelsOpen.Count + " levels available.");
        SaveData();
    }//End Achieve Level

    //add to Achievments
    public static void achieve(HazardClass h)
    {
        if (!achievementWall.Contains(h.type))
            achievementWall.Add(h.type);
    }

    //Grab All Hazards in current scene
    public void grabHazards()
    {
        Debug.Log("PlayerStats.grabHazards()");
        activeHazards = new ArrayList(GameObject.FindGameObjectsWithTag("hazard"));
        HazardClass hCur;
        HazardControllerClass hcCur;
        foreach (GameObject go in activeHazards)
        {
            //Debug to checking preloaded hazards
            print("A " + go.name + " was found in the scene.\n");
            hCur = (HazardClass)go.GetComponent("HazardClass");
            hcCur = (HazardControllerClass)go.GetComponent("HazardControllerClass");
            //Debug to check preloaded hazards
            print("It is a " + hCur.type + " type of Hazard.\n");
            print("It is currently solved:  " + hcCur.getIsSolved() + "\n");
        }
    }//End Grab Hazards



    public static void checkHazards()
    {
        print("Checking " + activeHazards.Count + " hazards");
        GameObject go;
        for (int i = 0; i < activeHazards.Count; i++)
        {
            go = (GameObject)activeHazards[i];
            Debug.Log("Is it solved?  " + ((HazardControllerClass)go.GetComponent("HazardControllerClass")).getIsSolved().ToString());
            if (((HazardControllerClass)go.GetComponent("HazardControllerClass")).getIsSolved())
            {
                HazardClass hCur = (HazardClass)go.GetComponent("HazardClass");
                //if not collected thus far add it to final list
                if (!achievementWall.Contains(hCur.type))
                {
                    achievementWall.Add(hCur.type);
                    print("New Acheivement unlocked");
                }

                //Add Hazard Object To ArrayList
                currentHazardsCollected.Add(go.name);
                //Add Hazard Type To ArrayList
                currentHazardsCollected.Add(hCur.type);
                print("An object with " + hCur.type + " has been collected.");
                //Add Completion Time To ArrayList
                currentHazardsCollected.Add(hCur.grabLife());
                //Remove Gameobject from active hazards list
                activeHazards.Remove(go);
            }

        }
    }



    //Set Scene
    public static void setScene(int x)
    {
        activeScene = x-5;
        print("Scene has been set.\n");
    }//End Set Scene



    //Set Defaults if Text Document Doesn't Exist
    public static void defaults()
    {
        currentHazardsCollected = new ArrayList();
        levelsOpen = new ArrayList();
        achievementWall = new ArrayList();
        /*
         * NOTE: CURRENT PROJECT SETTINGS HAS LEVEL 1 SCENE @ AN INDEX OF 6
         *     by deafault the levelsOpen array will contain one value: <6>
        */
        LoadData();
    }



    /*
     ***************************** STORING AND READING DATA:(3/13/2016) REVISED(4/11/2016) ********************************
     *
     *  <+> an arraylist containing Level Data named "levelsOpen" (integers for each level that is open)
     *  example: <0,1,2,3> levels 1-4 are open, but level 5 is not
     *  
     * NOTE: CURRENT PROJECT SETTINGS HAS LEVEL 1 SCENE @ AN INDEX OF 6
     *     by default the levelsOpen array will contain one value: <6>
     *  
     *  <+> an arraylist containing current level data is named "currentHazardsCollected".
     *  example:
     *             <Outlet,Electrical,0.45,Sink,Water,1.55>
     *             or
     *             <Knife,Weapon,0.25,Lighter,Fire,2.56,.....>
     *             
     * Each Data value is contained within 2 indicies:
     *             <Hazard Name, Hazard Type, Completion Time>
     * These Data Pairs are organized by when the Hazard Object is collected
     *             <Sink, Water, 2.0, Lighter, Fire, 3.24> 
     * The Water Hazard was identified and completed prior to the Fire Hazard
     * Water Hazard took 2.0 minutes to complete and was the sink object while the 
     * Fire Hazard took 3.24 Minutes and was a lighter object.
     * 
     * Each Array List will be stored in the text document, Each Array will begin with
     * '<' and end with '>'
     * 
     * Each document contains a different levels data
     * 
     * The first data to be read in will be the Level Data and stored in it's ArralyList
     * This ArrayList will be noted as "levelsOpen"
     * 
     * The next data to be read in will be the Achievement Data and stored in it's ArralyList
     * This ArrayList will be noted as "achievementWall"
     */



    //Load In All Values from text document (Load Data)
    public static void LoadData()
    {
        print("Data is being Loaded.\n");
        string data = loadFile.fileLoader.loadProgress();
        print("data: " + data);
        string temp;
        try
        {
            temp = data.Substring(1, data.IndexOf(',') - 1);
            data = data.Substring(data.IndexOf(',') + 1);
        }
        catch (Exception e)
        {
            temp = data.Substring(1, data.IndexOf('>') - 1);
            data = data.Substring(data.IndexOf('>') + 1);
        }

        print("parse temp: " + temp);
        print("remaining data: " + data);
        levelsOpen.Clear();
        levelsOpen.Add(int.Parse(temp));//get first value
        if (!data.StartsWith("<"))
        while (true)//unlocked levels
        {
            if (data.Contains(","))
            {
                temp = data.Substring(0, data.IndexOf(','));
                print("Level: " + temp + " added to open levels.");
                try
                {
                    levelsOpen.Add(int.Parse(temp));
                    data = data.Substring(temp.Length + 1);
                }
                catch (Exception) { break; }
            }
            else// at the end of the array
            {
                temp = data.Substring(0, data.IndexOf('>'));
                data = data.Substring(temp.Length + 1); 
                print("Level: " + temp + " added to open levels.");
                levelsOpen.Add(int.Parse(temp));
                break;
            }
            print("parse temp: " + temp);
            print("remaining data: " + data);
        }
        data = data.Substring(1);
        print("Data for achieves:" + data);
        if(!data.Equals(">"))
        while (true)//achievement wall
        {
            if (data.Contains("-"))
            {
                temp = data.Substring(0, data.IndexOf('-'));
                achievementWall.Add(temp);
                try
                {
                    data = data.Substring(temp.Length + 1);
                }
                catch (Exception) { break; }
            }
            else// at the end of the array
            { 
                temp = data.Substring(0, data.IndexOf('>'));
                print("temp:" + temp);
                achievementWall.Add(temp);
                break;
            }
            print("parse temp: " + temp);
            print("remaining data: " + data);
        }


        print("Loading Data has completed.\n");
    }//End Load Data

    public void ExportData()
    {
        loadFile.fileLoader.loadData();
    }

    //Save All Data to Text Document (Save Data)
    private static void SaveData()
    {
        //Debug to checking saving
        print("PlayerStats.SaveData()\n");

        string data = "<"; //Start Bracket for levelsOpen Data
        //Parse Levels Open Data to text
        for (int i = 0; i < levelsOpen.Count; i++)
        {
            if (i < levelsOpen.Count - 1)
                data = data + levelsOpen[i] + ",";   // add number of level and comma
            else
                data = data + levelsOpen[i];        //add number without comma (last element)
        }

        data += "><";   //End Bracket for levelsOpen Data
        for (int i = 0; i < achievementWall.Count; i++)
        {
            if (i < achievementWall.Count - 1)
                data = data + achievementWall[i] + "-";   // add number of level and comma
            else
                data = data + achievementWall[i];        //add number without comma (last element)
        }
        data += ">";
        saveFile.fileSaver.saveProgress(data); //overall game progress

        /* 
         Data is as follows:
         
          <Hazard Object,HazardType,Time,Hazard Object,HazardType,Time>
          
         */

        //Parse Collected Hazard Data to text
        //Reset String for Other Data
        data ="<";   //Start Bracket for hazards Collected Data
        for (int i = 0; i < currentHazardsCollected.Count; i++)       //Go through Main Array
        {
            Debug.Log("In the loop");
            
                if (i < currentHazardsCollected.Count - 1)
                    data = data + currentHazardsCollected[i] + ",";    //add Hazard Type or completion time with comma
                else
                    data = data + currentHazardsCollected[i].ToString();          //add Hazard Type or completion time without comma (last element)
            
        }
        data += ">";   //End Bracket for each hazards Collected Data line
        print("Data to be saved for research" + data);
        
        saveFile.fileSaver.saveData(data,activeScene + "-"+ DateTime.Now.ToString("yyyy_mm_dd_hh_mm_ss")); // data to be looked at
        //Debug to checking saving
        print("Data has been saved.\n");
    }//End Save Data

}
