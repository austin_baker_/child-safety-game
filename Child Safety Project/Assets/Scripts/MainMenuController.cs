﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuController : MonoBehaviour {


    public void pressStartGame()
    {
        // We will probably load the user data at this point
        SceneManager.LoadScene("LevelSelect");
    }

    public void pressTokens()
    {
        SceneManager.LoadScene("TokensMenu");
    }

    public void pressExit()
    {
        // We may have to save the data as we exit the game.
        // Not sure how we want to go about doing this.
        Application.Quit();
    }

    public void pressOptions()
    {
        // The options menu might be within the same scene as the Main Menu
        SceneManager.LoadScene("OptionsMenu");
    }

}
