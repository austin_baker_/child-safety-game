﻿using UnityEngine;
using System.Collections;

public class HazardControllerClass : MonoBehaviour {

    public string hazardDescription = "This is just a hazard, bro.";
    public AudioClip hazardAudio;
    public GameObject tokenObject;
    public Sprite tokenSprite;

    //Whether or not the player has solved the problem.  By default they haven't.
    private bool isSolved = false;
    //The TaskData object associated with this hazard
    //private TaskData taskData
    public void setIsSolved (bool sol)
    {
        isSolved = sol;
        if(isSolved == true)
        {
            //perform some operation on taskData
        }
        else if(isSolved == false)
        {
            //perform some operation on taskData
        }
    }
    public bool getIsSolved()
    {
        return isSolved;
    }

}
