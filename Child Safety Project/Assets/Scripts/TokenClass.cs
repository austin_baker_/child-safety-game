﻿using UnityEngine;
using System.Collections;

public class TokenClass : MonoBehaviour {

    private PlayerUIController PlayerUI;
    public float rotateSpeed = 1f;      //The speed at which the object rotates, in Degrees/FixedUpdate
    public float floatAcc = 1.0f;       //The rate at which the object floats up and down
    public float initFloatSpeed = 0.5f;     //The initial speed the token is moving up or down
    public float fadeRate = 1.0f;       //The rate at which the object fades in and out
    public Renderer side1;              //Renderer of the quad object
    public Renderer side2;              //Renderer of the quad object

    private float startHeight;          //The initial y position of the token
    private float floatSpeed;           //The current floating speed of the token
    private bool isVisible = false;     //The current state of the object's visibility
    private HazardControllerClass parentController;

    ////////////////////////////
    //  OPERATIONAL FUNCTIONS //
    ////////////////////////////

    public void collectToken()
    {
        Debug.Log("Collected token!");

        //Jeremy's Code
        HazardClass h;
        GameObject g = this.gameObject; //grab current game object (token)
        g = g.transform.parent.gameObject;  //grab parent object (hazard)
        h = (HazardClass)g.GetComponent("HazardClass"); //grab hazard script off of object
        //PlayerStats.achieve(h);
        //PlayerStats.currentHazardsCollected.Add(h.type);
        //PlayerStats.currentHazardsCollected.Add(h.grabLife());
        //End Jeremy's Code

        
        PlayerUI.collectedToken(
            gameObject.transform.parent.name, 
            parentController.hazardDescription, 
            parentController.tokenSprite,
            parentController.hazardAudio
        );

        //Tell the parent that the hazard was solved
        ((HazardControllerClass)this.gameObject.transform.parent.transform.GetComponent("HazardControllerClass")).setIsSolved(true);
        

		Transform[] siblings = transform.parent.GetComponentsInChildren<Transform>();
		int maxIndex = siblings.Length;
		for (int i = 1; i < maxIndex; i++) {
            Debug.Log(siblings[i].gameObject);
			Destroy(siblings [i].gameObject);
		}
        // update the stats
        PlayerStats.checkHazards();
    }

    ///////////////////////////
    //  ANIMATION FUNCTIONS  //
    ///////////////////////////

    //Rotates the object based on its preset rotateSpeed
    private void rotateToken()
    {
        this.gameObject.transform.Rotate(new Vector3(0, 1, 0), rotateSpeed);
    }

    //Gently moves the token up and down consistently
    private void floatToken()
    {
        if (transform.position.y > startHeight)
        {
            floatSpeed -= floatAcc;
        }
        else
        {
            floatSpeed += floatAcc;
        }

        
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + floatSpeed, this.transform.position.z);
    }

    //Fades the Quads of the token into full Opacity
    private void fadeIn()
    {
        if (side1.material.color.a < 1)
        {
            side1.material.color = new Color(1, 1, 1, side1.material.color.a + fadeRate);
        }
        if (side2.material.color.a < 1)
        {
            side2.material.color = new Color(1, 1, 1, side2.material.color.a + fadeRate);
        }
    }

    //Fades the Quads of the token into full Transparency
    private void fadeOut()
    {
        if (side1.material.color.a > 0)
        {
            side1.material.color = new Color(1, 1, 1, side1.material.color.a - fadeRate);
        }
        if (side2.material.color.a > 0)
        {
            side2.material.color = new Color(1, 1, 1, side2.material.color.a - fadeRate);
        }
    }


    ///////////////////////
    /// UNITY FUNCTIONS ///
    ///////////////////////

    void Start()
    {
        floatSpeed = initFloatSpeed;
        startHeight = this.transform.position.y;
        isVisible = false;
        PlayerUI = GameObject.FindGameObjectWithTag("MVC").transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<PlayerUIController>();
        parentController = gameObject.GetComponentInParent<HazardControllerClass>();
    }

    void FixedUpdate()
    {
        //Rotating Animation
        rotateToken();
        
        //Floating Animation
        floatToken();

        //Visibility
        if (isVisible)
        {
            fadeIn();
        }
        else
        {
            fadeOut();
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "player")
        {
            isVisible = true;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "player")
        {
            isVisible = false;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.collider.gameObject.tag == "player")
        {
            //collectToken();
            
        }
    }
	
}
