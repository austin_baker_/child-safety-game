﻿using UnityEngine;
using System.Collections;

public class StairGenerator : MonoBehaviour {

    public Transform Ramp;         // collision object that defines the length, width and angle of the stairs
    public Transform Step;         // non collision prefab that defines the way each step will look
    public int numOfSteps = 7;


	
    // Generate the number of steps with the given angle and position of the Ramp
    void generateSteps()
    {
        // Check the number of steps
        if (numOfSteps <= 0)
        {
            Debug.LogError("Invalid number of steps");
            Destroy(Step);
        }

        // Define the parameters for each step
        float angle = 2 * Mathf.PI / 360 * Ramp.localEulerAngles.x;
        float rampLength = Ramp.localScale.z;
        float rampWidth = Ramp.localScale.x;
        float baseLength = rampLength * Mathf.Abs(Mathf.Cos(angle));
        float baseHeight = rampLength * Mathf.Abs(Mathf.Sin(angle));
        float stepLength = baseLength / numOfSteps;
        float stepHeight = baseHeight / numOfSteps;


        // Initialize the drawing cursor position
        Vector3 cursor = new Vector3( 0, -baseHeight / 2 + (stepHeight / 2), -baseLength / 2 + (stepLength / 2));
        Quaternion rotation = GetComponent<Transform>().rotation;

        // Generate the steps accordingly
        for (int i = 0; i < numOfSteps; i++)
        {
            Transform stp = Instantiate(Step, cursor, rotation) as Transform;       //create a new step
            stp.parent = GetComponent<Transform>();                                 //set this object as the parent
            stp.localScale = new Vector3(rampWidth, stepHeight, stepLength);        //set the size accordingly
            stp.localPosition = cursor;                                             //position the step accordinly
            cursor = new Vector3(cursor.x, cursor.y + stepHeight, cursor.z + stepLength); //advance the cursor
        }
    }

	void Start () {
        Ramp.GetComponent<Renderer>().enabled = false;
        generateSteps();
	}
	
}
